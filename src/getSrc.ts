const states = [
  'On.png',
  'Off.png',
  'DownOn.png',
  'DownOff.png',
  'RollOn.png',
  'RollOff.png',
]

export default function getSrc(element: Cheerio, state: number) {
  return element.attr('imageName') + states[state]
}
