import fs from 'fs'
import { resolve, sep } from 'path'
import load from './load'
import convert from './convert'
import generate from './generate'

const cwd = process.cwd()
const sourceDir = resolve(cwd, process.argv[2])
const formName = sourceDir.split(sep).pop()
const formPath = resolve(sourceDir, 'form.html')

export default function main() {
  const html = load(formPath)
  const { dom, style } = convert(html, formName)
  const component = generate(dom, style)

  fs.writeFileSync(resolve(__dirname, '..', 'result.vue'), component)
}
