export default function generate(dom: string, style: string) {
  return `<template>
  <div>${dom}</div>
</template>

<script>
import formMixin from './formMixin'

export default {
  mixins: [formMixin]
}
</script>

<style scoped>
${style}
</style>
  `
}
